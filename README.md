# Angular Application #

Interactive - **http://angular-s.16mb.com/**

## The Task ##

```


Написать онлайн библиотеку. 
Есть 3 страницы:
- На главной странице пользователь просто видит топ 10 новых книг. Топ 10 авторов. своего рода дашборд такой.
- На странице поиска книг:
У пользователя должна быть возможность, сделать поиск по книгам, посмотреть детали по выбранной книге, отсортировать книги (по названию, по популярности)
- На страницы "просмотра" книги, у пользователя должна быть возможность откомментировать книги. поставить рейтинг. Отобразить картинку для книги.
- Страница добавления новой книги. Тоже самое что и на странице просмотра, только все поля редактируемые, картинка может быть загружена.

Используемые фреймверки:
- angularjs
- angular-strap
- sails.js (для моков)
- restangular
- остальное на свое усмотрение
```


## Dashboard ##
![photo_2016-01-12_17-00-37.jpg](https://bitbucket.org/repo/j6B6EE/images/281418805-photo_2016-01-12_17-00-37.jpg)
## Add new book ##
![addbook.png](https://bitbucket.org/repo/j6B6EE/images/3133720811-addbook.png)
## Add new book success ##
![addbooksuccess.png](https://bitbucket.org/repo/j6B6EE/images/3330445086-addbooksuccess.png)
## Book information ##
![book.png](https://bitbucket.org/repo/j6B6EE/images/3386592354-book.png)
## Author information ##
![author.png](https://bitbucket.org/repo/j6B6EE/images/920371754-author.png)