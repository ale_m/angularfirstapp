/**
 * BookController
 *
 * @description :: Server-side logic for managing books
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    upload: function (req, res) {
        if (req.method === 'GET')
            return res.json({
                'status': 'GET not allowed'
            });

        var uploadFile = req.file('uploadFile');

        uploadFile.upload({
            dirname: '../../assets/images',
            saveAs: function (file, cb) {
                cb(null, file.filename);
            }
        }, function onUploadComplete(err, files) {

            if (err) return res.serverError(err);

            res.json({
                status: 200,
                file: files
            });
        });
    }
};
