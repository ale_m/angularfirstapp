/**
 * Author.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {
        name: {
            type: 'string',
            required: true
        },
        birthday: {
            type: 'date',
            required: false
        },
        biography: {
            type: 'string',
            required: false
        },
        books: {
            collection: 'book',
            via: 'author'
        },
        votes: {
            collection: 'authorVote',
            via: 'author'
        },
        rating: function () {
            var sum = this.votes.reduce(function (a, b) {
                return a + b;
            });
            return sum / this.votes.length;
        }
    }
};