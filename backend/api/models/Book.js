/**
 * Book.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {
        name: {
            type: 'string',
            required: true
        },
        description: {
            type: 'string',
            required: false
        },
        year: {
            type: 'integer',
            required: false
        },
        cover: {
            type: 'string',
            required: false
        },
        author: {
            model: 'author'
        },
        comments: {
            collection: 'comment',
            via: 'book'
        },
        votes: {
            collection: 'bookVote',
            via: 'book'
        },
        rating: {
            type: 'integer',
            required: false
        },
    	toJSON : function(){
      	    var obj = this.toObject();
	    var count = 0;

            for(var i = 0; i < this.votes.length; i++)
            {
            	count = count + this.votes[i].value;
            }
	    obj.rating = count / this.votes.length;

            return obj;
    	}
    }
};
