/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {
        name: {
            type: 'string',
            required: true
        },
        email: {
            type: 'string',
            email: true,
            required: true
        },
        comments: {
            collection: 'comment',
            via: 'user'
        },
        authorVotes: {
            collection: 'authorVote',
            via: 'user'
        },
        bookVotes: {
            collection: 'bookVote',
            via: 'user'
        }
    }
};