require.config({
    paths: {
        'jquery': '../vendors/js/jquery.min',
        'bootstrap': '../vendors/js/bootstrap.min',
        //
        'angular': '../vendors/js/angular.min',
        //
        'angular-route': '../vendors/js/angular-route.min',
        //
        'restangular': '../vendors/js/restangular.min',
        'underscore': '../vendors/js/underscore-min',
        //
        'angular-strap': '../vendors/js/angular-strap.min',
        'angular-strap-tpl': '../vendors/js/angular-strap.tpl.min',
        //
        'angular-file-upload': '../vendors/js/ng-file-upload-all.min',
        //
        'config': 'module'
    },
    shim: {
        'angular': {
            exports: 'angular',
            deps: ['jquery', 'bootstrap']
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'angular-route': {
            exports: 'ngRoute',
            deps: ['angular']
        },
        'underscore': {
            exports: '_'
        },
        'restangular': {
            exports: 'Restangular',
            deps: ['angular', 'underscore']
        },
        'angular-strap': {
            deps: ['angular']
        },
        'angular-strap-tpl': {
            deps: ['angular-strap']
        },
        'angular-file-upload': {
            deps: ['angular']
        },
        'module': {
            deps: ['angular']
        }
    }
});