define(function () {
    'use strict';

    function NavBarDirective() {
        return {
            restrict: 'E',
            controller: "AddBookController",
            templateUrl: 'js/templates/nav.html',
            transclude: true
        }
    }
    return NavBarDirective;
});