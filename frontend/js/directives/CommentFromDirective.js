define(function () {
    'use strict';

    function CommentFormDirective() {
        return {
            restrict: 'E',
            controller: "AddCommentController",
            templateUrl: 'js/templates/comment_form.html',
            transclude: true
        }
    }
    return CommentFormDirective;
});