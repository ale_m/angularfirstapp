define(['main'], function (libApp) {
    'use strict';

    libApp
        .config(function (RestangularProvider) {
            RestangularProvider.setBaseUrl('http://localhost:1337');
        })
        .config(function ($routeProvider, $locationProvider) {
            $routeProvider
                .when('/book/:id', {
                    templateUrl: 'js/templates/book.html',
                    controller: 'BookController'
                })
                .when('/author/:id', {
                    templateUrl: 'js/templates/author.html',
                    controller: 'AuthorController'
                })
                .when('/top', {
                    templateUrl: 'js/templates/top.html',
                    controller: 'RatingController'
                })
                .when('/search', {
                    templateUrl: 'js/templates/search.html',
                    controller: 'SearchController'
                })
                .otherwise({
                    redirectTo: '/top'
                });
        });
    console.log('The application is configured...');
});