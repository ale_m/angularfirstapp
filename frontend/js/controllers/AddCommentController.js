define(function () {
    'use strict';

    function AddCommentController($scope, Comment, $alert) {

        $scope.data = {};

        $scope.addComment = function () {
            Comment.add(getParam()).then(success.bind(this));
        }

        function getParam() {

            var param = {
                text: $scope.data.comment,
                user: _.where($scope.users, {
                    name: $scope.data.user
                })[0]['id'],
                book: $scope.book.id
            };

            return param;

        }

        function success(response) {

            var newObj = {
                text: response.text,
                user: _.where($scope.users, {
                    name: $scope.data.user
                })[0],
                book: response.book
            };

            $scope.data.result = true;
            $scope.book.comments.push(newObj);
        }
    }

    return AddCommentController;
});