define(function () {
    'use strict';

    function SearchController($scope, Book) {

        $scope.data = {};
        $scope.sortType = 'name';
        $scope.sortReverse = false;

        $scope.findBook = function () {

            var param = {
                "name": {
                    "contains": $scope.data.book
                }
            };

            Book.find(param).then(setScope.bind(this, 'books'));
        };

        $scope.showInfo = setScope.bind(this, 'bookHover'); 

        function setScope(key, value) {
            $scope[key] = value;
        }
    }

    return SearchController;
});