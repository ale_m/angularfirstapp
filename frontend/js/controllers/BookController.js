define(function () {
    'use strict';

    function BookController($scope, $routeParams, Book, User, Vote) {

        var queryId = $routeParams.id;

        User.getAll().then(setScope.bind(this, 'users'));

        Book.get(queryId).then(successGetBook.bind(this));

        $scope.addVote = function () {
            Vote.add(getParam()).then(successVote.bind(this));
        }

        function getParam() {

            var param = {
                value: $scope.data.vote,
                book: $scope.book.id,
                user: _.where($scope.users, {
                    name: $scope.data.userVote
                })[0]['id']
            };

            return param;
        }

        function successGetBook(response) {
            
            $scope.book = response;

            _.each($scope.book.comments, function (comment) {
                comment.user = _.where($scope.users, {
                    id: comment.user
                })[0];
            });
        }

        function successVote(response) {
            $scope.data.resultVote = true;
        }

        function setScope(key, value) {
            $scope[key] = value;
        }
    }
    return BookController;
});