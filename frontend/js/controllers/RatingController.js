define(function () {
    'use strict';

    function RatingController($scope, Author, Book) {

        var queryParam = {
            limit: 10,
            sort: 'rating ASC'
        };

        Book.getAll(queryParam).then(setScope.bind(this, 'books'));

        Author.getAll(queryParam).then(setScope.bind(this, 'authors'));

        function setScope(key, value) {
            $scope[key] = value;
        }
    }
    return RatingController;
});