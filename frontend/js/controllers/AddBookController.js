define(function () {
    'use strict';

    function AddBookController($scope, $modal, Upload, Author, Book) {

        var modal = $modal({
                scope: $scope,
                templateUrl: 'js/templates/modal.html',
                show: false
            }),
            queryParam = {
                sort: 'name ASC'
            };

        $scope.showModal = function () {
            modal.$promise.then(modal.hide);
        };

        $scope.data = {};

        Author.getAll(queryParam).then(setScope.bind(this, 'authors'));

        $scope.addBooks = function () {

            var param = {
                name: $scope.data.name,
                year: $scope.data.year,
                author: _.where($scope.authors, {
                    name: $scope.data.author
                })[0]['id'],
                description: $scope.data.summary
            };

            Book.add(param).then(successAddBook.bind(this));
        }

        function successAddBook(response) {
            var paramCover = {
                url: 'http://localhost:1337/book/upload',
                data: {
                    uploadFile: $scope.data.cover
                }
            };

            $scope.data.resultBook = true;

            if ($scope.data.cover) {
                Upload.upload(paramCover).then(bookAddCover.bind(this, response.id));
            }
        }

        function bookAddCover(id, response) {

            var cover = {
                cover: 'images/' + response.data.file[0].filename
            };

            Book.update(id, cover).then(function (response) {
                $scope.data.resultCover = true;
            });
        }

        function setScope(key, value) {
            $scope[key] = value;
        }
    }

    return AddBookController;
});