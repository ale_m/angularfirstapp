define(function () {
    'use strict';

    function AuthorController($scope, $routeParams, Author) {

        var queryId = $routeParams.id;

        Author.get(queryId).then(setScope.bind(this, 'author'));

        function setScope(key, value) {
            $scope[key] = value;
        }
    }

    return AuthorController;
});