define('main', ['controllers/AddBookController',
                'controllers/RatingController',
                'controllers/BookController',
                'controllers/AuthorController',
                'controllers/SearchController',
                'controllers/AddCommentController',
                'directives/NavBarDirective',
                'directives/CommentFromDirective',
                'services/BookService',
                'services/UserService',
                'services/AuthorService',
                'services/CommentService',
                'services/BookVoteService',
                'angular-route',
                'restangular',
                'angular-strap-tpl',
                'angular-file-upload'],
    function (AddBookController, RatingController, BookController, AuthorController, SearchController, AddCommentController, NavBarDirective, CommentFormDirective,
        BookService, UserService, AuthorService, CommentService, BookVoteService) {
        'use strict';

        var libApp = angular.module('libApp', ['ngRoute', 'restangular', 'mgcrea.ngStrap', 'ngFileUpload']);

        libApp
            .directive('navBar', [NavBarDirective])
            .directive('commentForm', [CommentFormDirective])
            .service('BookService', ['Restangular', BookService])
            .service('UserService', ['Restangular', UserService])
            .service('AuthorService', ['Restangular', AuthorService])
            .service('CommentService', ['Restangular', CommentService])
            .service('BookVoteService', ['Restangular', BookVoteService])
            .controller('RatingController', ['$scope', 'AuthorService', 'BookService', RatingController])
            .controller('BookController', ['$scope', '$routeParams', 'BookService', 'UserService', 'BookVoteService', BookController])
            .controller('AuthorController', ['$scope', '$routeParams', 'AuthorService', AuthorController])
            .controller('SearchController', ['$scope', 'BookService', SearchController])
            .controller('AddBookController', ['$scope', '$modal', 'Upload', 'AuthorService', 'BookService', AddBookController])
            .controller('AddCommentController', ['$scope', 'CommentService', AddCommentController]);

        console.log('The application is created...');

        return libApp;
    });

require(['requirejs.config'], function () {
    require(['config'], function () {
        angular.bootstrap(document.body, ['libApp']);
        console.log('The application is running...');
    });
});