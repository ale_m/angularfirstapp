define(function () {
    'use strict';

    function UserService(Restangular) {

        this.getAll = function () {

            var promise = Restangular.all('user').getList().then(function (response) {
                return response;
            });

            return promise;
        }

    }

    return UserService;
});