define(function () {
    'use strict';

    function BookVoteService(Restangular) {

        this.add = function (param) {

            var promise = Restangular.all("bookvote").get("create", param).then(function (response) {
                return response;
            });

            return promise;
        }
    }

    return BookVoteService;
});