define(function () {
    'use strict';

    function AuthorService(Restangular) {

        this.get = function (id) {

                var promise = Restangular.one('author', id).get().then(function (response) {
                    return response;
                });

                return promise;
            },
            this.getAll = function (param) {

                var promise = Restangular.all('author').getList(param).then(function (response) {
                    return response;
                });

                return promise;
            };

    }

    return AuthorService;
});