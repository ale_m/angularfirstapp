define(function () {
    'use strict';

    function CommentService(Restangular) {

        this.add = function (param) {

            var promise = Restangular.all("comment").get("create", param).then(function (response) {
                return response;
            });

            return promise;
        }
    }

    return CommentService;
});