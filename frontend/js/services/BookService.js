define(function () {
    'use strict';

    function BookService(Restangular) {

        this.get = function (id) {
                var promise = Restangular.one('book', id).get().then(function (response) {
                    return response;
                });

                return promise;
            },

            this.getAll = function (param) {
                var promise = Restangular.all('book').getList(param).then(function (response) {
                    return response;
                });

                return promise;
            },

            this.add = function (param) {
                var promise = Restangular.all("book").get("create", param).then(function (response) {
                    return response;
                });

                return promise;
            },

            this.find = function (param) {
                var promise = Restangular.all("book").getList({
                    where: param
                }).then(function (response) {
                    return response;
                });
                return promise;
            },

            this.update = function (id, param) {
                var promise = Restangular.one("book", "update").customGET(id, param).then(function (response) {
                    return response;
                });

                return promise;
            }
    }

    return BookService;
});