module.exports = function (grunt) {
    grunt.initConfig({
        less: {
            development: {
                files: {
                    "css/style.css": "less/style.less"
                }
            },
            production: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    "css/style.min.css": "less/style.less"
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-less');
//    grunt.registerTask('default', ['less:development']);
//    grunt.registerTask('prod', ['less:production']);
    grunt.registerTask('default', ['less']);
};